using little_bean_toll.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Interface;
using little_bean_toll.Service;

namespace little_bean_toll
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
               
                options.AddPolicy("CorsPolicy", policy =>
                {
                  
                    policy.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
            //Scaffold-DbContext "Server=NB004771\SQLEXPRESS;Database=little_bean;Trusted_Connection=True;User ID=;Password=" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -Force
            services.AddControllers();
            services.AddDbContext<little_beanContext>(options =>options.UseSqlServer(Configuration.GetConnectionString("TodoDatabase")));
            //services.AddSingleton<IStudentService, StudentService>();
            services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<IWeekTimeService, WeekTimeService>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("CorsPolicy");
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            // �M�� Policy �� Middleware
            
        }
    }
}
