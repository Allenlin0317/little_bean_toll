﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class StudentResponse : StudentInfo
    {
        public new string SexString { get; set; }

        public new string TypeString { get; set; }

        public new string PayTypeString { get; set; }
    }
}
