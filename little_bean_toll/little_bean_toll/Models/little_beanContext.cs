﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class little_beanContext : DbContext
    {
        public little_beanContext()
        {
        }

        public little_beanContext(DbContextOptions<little_beanContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Charged> Chargeds { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<CourseTime> CourseTimes { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<Leave> Leaves { get; set; }
        public virtual DbSet<Meal> Meals { get; set; }
        public virtual DbSet<MealDate> MealDates { get; set; }
        public virtual DbSet<MealWeek> MealWeeks { get; set; }
        public virtual DbSet<StudentCourse> StudentCourses { get; set; }
        public virtual DbSet<StudentInfo> StudentInfos { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }
        public virtual DbSet<Tuition> Tuitions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Chinese_Taiwan_Stroke_CI_AS");

            modelBuilder.Entity<Charged>(entity =>
            {
                entity.ToTable("Charged");

                entity.Property(e => e.CreatedOn)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasDefaultValueSql("(getdate())")
                    .IsFixedLength(true);

                entity.Property(e => e.ModifiedOn)
                    .HasMaxLength(10)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<Course>(entity =>
            {
                entity.ToTable("Course");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.TeacherId).HasColumnName("TeacherID");
            });

            modelBuilder.Entity<CourseTime>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.WeekDay, e.StartTime })
                    .HasName("PK_CourseTime_1");

                entity.ToTable("CourseTime");

                entity.Property(e => e.WeekDay).HasMaxLength(10);

                entity.Property(e => e.StartTime).HasMaxLength(10);

                entity.Property(e => e.EndTime)
                    .IsRequired()
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<Discount>(entity =>
            {
                entity.ToTable("Discount");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Discount1).HasColumnName("Discount");

                entity.Property(e => e.DiscountReason).HasMaxLength(100);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.YearMonth)
                    .IsRequired()
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<Leave>(entity =>
            {
                entity.ToTable("Leave");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LeaveCourse)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LeaveDate)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Reason).HasMaxLength(50);

                entity.Property(e => e.YearMonth)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<Meal>(entity =>
            {
                entity.ToTable("Meal");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<MealDate>(entity =>
            {
                entity.HasKey(e => new { e.Student, e.Date, e.YearMonth })
                    .HasName("PK_MealCount");

                entity.ToTable("MealDate");

                entity.Property(e => e.Date).HasMaxLength(50);

                entity.Property(e => e.YearMonth).HasMaxLength(20);
            });

            modelBuilder.Entity<MealWeek>(entity =>
            {
                entity.HasKey(e => new { e.Student, e.Week, e.YearMonth });

                entity.ToTable("MealWeek");

                entity.Property(e => e.Week).HasMaxLength(10);

                entity.Property(e => e.YearMonth).HasMaxLength(20);
            });

            modelBuilder.Entity<StudentCourse>(entity =>
            {
                entity.HasKey(e => new { e.Student, e.Course });

                entity.ToTable("StudentCourse");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StatusReason).HasMaxLength(50);
            });

            modelBuilder.Entity<StudentInfo>(entity =>
            {
                entity.ToTable("StudentInfo");

                entity.Property(e => e.Class).HasMaxLength(5);

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.ParentsName).HasMaxLength(10);

                entity.Property(e => e.ParentsPhone).HasMaxLength(22);

                entity.Property(e => e.Phone).HasMaxLength(22);

                entity.Property(e => e.School)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Sibling).HasMaxLength(100);

                entity.Property(e => e.Tel).HasMaxLength(22);
            });

            modelBuilder.Entity<Teacher>(entity =>
            {
                entity.ToTable("Teacher");

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Tel)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Tuition>(entity =>
            {
                entity.ToTable("Tuition");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedOn)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Tuition1).HasColumnName("Tuition");

                entity.Property(e => e.YearMonth)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
