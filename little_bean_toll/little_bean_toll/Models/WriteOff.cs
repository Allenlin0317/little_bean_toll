﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class WriteOff
    {
        public int Id { get; set; }
        public int Student { get; set; }
        public int IsWirteOff { get; set; }
        public int WriteOffPerson { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
    }
}
