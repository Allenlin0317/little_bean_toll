﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class StudentInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Sex { get; set; }
        public string Tel { get; set; }
        public string Phone { get; set; }
        public string ParentsName { get; set; }
        public string ParentsPhone { get; set; }
        public string School { get; set; }
        public string Class { get; set; }
        public int Type { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int Grade { get; set; }
        public string Sibling { get; set; }
        public int? PayType { get; set; }
        public int? IsPrepaid { get; set; }
        public int? CountPrepaid { get; set; }
    }
}
