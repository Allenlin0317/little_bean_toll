﻿using System;
using System.Collections.Generic;
using little_bean_toll.Enums;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class CourseInfo
    {
        public int Id { get; set; }
        public int[] StudentCourseId { get; set; }
        public int StudentId { get; set; }
        public int Grade { get; set; }
        public string CourseName { get; set; }
        public string TeacherName { get; set; }
        public int TeacherId { get; set; }
        public CourseTime CourseTime { get; set; }
        public EnumClassType ClassType { get; set; }
        
        public string ClassTypeName => ClassType.ToString();

        public EnumCourseType CourseType { get; set; }
        public string CourseTypeName => CourseType.ToString();
        public string StudentCourseStatus { get; set; }
        public string StatusReason { get; set; }
    }
}
