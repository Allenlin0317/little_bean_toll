﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class Discount
    {
        public int Id { get; set; }
        public int Student { get; set; }
        public int Discount1 { get; set; }
        public string DiscountReason { get; set; }
        public int Type { get; set; }
        public string YearMonth { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
