﻿using System;
using System.Collections.Generic;
using little_bean_toll.Controllers;
using little_bean_toll.Enums;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class TuitionInfo
    {
        public int StudentId { get; set; }
        public int StudentGrade { get; set; }
        public int CourseSelect { get; set; }
        public string CourseName { get; set; }
        public string CourseTime { get; set; }
        public int Tuition { get; set; }

        //public int MealCount { get; set; }
        public string MealWeek { get; set; }
        public int Discount { get; set; }
        public string YearMonth { get; set; }
        public int Total { get; set; }
        public EnumDiscountType DiscountType { get; set; }
        public EnumStudentType StudentType { get; set; }
        public EnumPayType PayType { get; set; }
        public EnumCourseType CourseType { get; set; }
        public EnumClassType ClassType { get; set; }
        public EnumMealPayType MealPayType { get; set; }
        public EnumStudentCourseStatus StudentCourseStatus { get; set; }
}
}
