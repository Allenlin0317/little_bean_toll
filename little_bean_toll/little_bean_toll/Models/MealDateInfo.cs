﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class MealDateInfo
    {
        public int Student { get; set; }
        public string name { get; set; }
        public string Date { get; set; }
        public string Week { get; set; }
        public int Count { get; set; }
        public int Type { get; set; }
        public string YearMonth { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
