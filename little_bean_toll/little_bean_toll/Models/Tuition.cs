﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class Tuition
    {
        public int Id { get; set; }
        public int Student { get; set; }
        public int Tuition1 { get; set; }
        public int Meal { get; set; }
        public int Discount { get; set; }
        public int Total { get; set; }
        public string YearMonth { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
