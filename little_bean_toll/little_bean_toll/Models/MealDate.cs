﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class MealDate
    {
        public int Student { get; set; }
        public string Date { get; set; }
        public int Count { get; set; }
        public string YearMonth { get; set; }
    }
}
