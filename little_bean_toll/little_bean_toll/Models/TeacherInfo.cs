﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class TeacherInfo
    {
        public string TeacherName { get; set; }
        public string TeacherTel { get; set; }
        public string CourseName { get; set; }
        public List<CourseTime> CourseTime { get; set; }
    }
}
