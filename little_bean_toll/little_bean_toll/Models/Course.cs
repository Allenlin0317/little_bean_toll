﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class Course
    {
        public int Id { get; set; }
        public int Grade { get; set; }
        public string Name { get; set; }
        public int CourseType { get; set; }
        public int ClassType { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? TeacherId { get; set; }
    }
}
