﻿using little_bean_toll.Enums;
using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class MealInfo
    {
        public int Student { get; set; }
        public string Date { get; set; }
        public string Week { get; set; }
        public int Count { get; set; }
        public EnumMealPayType Type { get; set; }
        public string YearMonth { get; set; }
    }
}
