﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class Charged
    {
        public int Id { get; set; }
        public int Student { get; set; }
        public int IsCharged { get; set; }
        public int ChargedPerson { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
    }
}
