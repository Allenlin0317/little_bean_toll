﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class Leave
    {
        public int Id { get; set; }
        public int Student { get; set; }
        public string LeaveCourse { get; set; }
        public string LeaveDate { get; set; }
        public string Reason { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int Status { get; set; }
        public string YearMonth { get; set; }
    }
}
