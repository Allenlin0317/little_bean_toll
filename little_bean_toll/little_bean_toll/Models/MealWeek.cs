﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class MealWeek
    {
        public int Student { get; set; }
        public string Week { get; set; }
        public int Count { get; set; }
        public string YearMonth { get; set; }
    }
}
