﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class StudentCourse
    {
        public int Student { get; set; }
        public int Course { get; set; }
        public string StatusReason { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
