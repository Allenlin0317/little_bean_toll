﻿using System;
using System.Collections.Generic;

#nullable disable

namespace little_bean_toll.Models
{
    public partial class Meal
    {
        public int Id { get; set; }
        public int Student { get; set; }
        public int Type { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
