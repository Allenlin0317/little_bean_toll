﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace little_bean_toll.Dtos
{
    public class STUDENTINFODtos
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Sex { get; set; }
        public string Tel { get; set; }
        public string Phone { get; set; }
        public string ParentsName { get; set; }
        public string ParentsPhone { get; set; }
        public string School { get; set; }
        public string Class { get; set; }
        public long Type { get; set; }
    }
}
