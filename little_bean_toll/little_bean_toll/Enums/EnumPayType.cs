﻿namespace little_bean_toll.Enums
{
    public enum EnumPayType
    {
        月繳=1,
        雙月繳=2,
        季繳=4,
        半年繳=8,
        計次繳=16
    }
}