﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Enums;
using little_bean_toll.Interface;
using little_bean_toll.Models;
using Microsoft.EntityFrameworkCore;

namespace little_bean_toll.Service
{
    public class StudentService : IStudentService
    {
        private readonly little_beanContext _littleBeanContext;
        public StudentService(little_beanContext littleBeanContext)
        {
            _littleBeanContext = littleBeanContext;
        }

        public List<StudentResponse> CalculateStudentGrade(DbSet<StudentInfo> studentInfos)
        {
            var studentInfos1 = studentInfos.ToList();

            return studentInfos1.Select(x =>
            {
                var diffYear = Math.Abs(x.CreatedOn.Year - DateTime.Now.Year);
                x.Grade += IsCrossTerm(x) ? diffYear + 1 : diffYear;

                return new StudentResponse() { Grade = x.Grade, Class = x.Class, CountPrepaid = x.CountPrepaid, CreatedOn = x.CreatedOn, Id = x.Id, IsPrepaid = x.IsPrepaid, ModifiedOn = x.ModifiedOn, Name = x.Name, ParentsName = x.ParentsName, ParentsPhone = x.ParentsPhone,PayType = x.PayType, PayTypeString = Enum.Parse<EnumSexType>(x.PayType.ToString(), true).ToString(), Phone = x.Phone, School = x.School,Sex = x.Sex, SexString = Enum.Parse<EnumSexType>(x.Sex.ToString(), true).ToString(), Sibling = x.Sibling, Tel = x.Tel,Type = x.Type, TypeString = Enum.Parse<EnumSexType>(x.Type.ToString(), true).ToString() };
            }).ToList();
        }

        private static bool IsCrossTerm(StudentInfo studentInfo)
        {
            return studentInfo.CreatedOn.Month < 8;
        }

        public string GetStudentInfo(int Id)
        {
            var StudentName = from StudentInfo in _littleBeanContext.StudentInfos
                              where StudentInfo.Id == Id
                              select new { StudentInfo.Name };

            return StudentName.ToString();
        }
        public int GetStudentInfo(string Name)
        {
            var StudentId = from StudentInfo in _littleBeanContext.StudentInfos
                            where StudentInfo.Name.Contains(Name)
                            select new { StudentInfo.Id };

            return Convert.ToInt32(StudentId.FirstOrDefault().Id);
        }

    }
}
