﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Enums;
using little_bean_toll.Interface;
using little_bean_toll.Models;
using Microsoft.EntityFrameworkCore;


namespace little_bean_toll.Service
{
    public class TuitioneService : ITuitionService
    {
        private readonly little_beanContext _littleBeanContext;
        private Dictionary<string, int> MajorMonthPriceMapping = new Dictionary<string, int>() { { "英文", 2300 }, { "數學", 2300 }, { "理化", 2300 }, { "語文", 1500 }, { "生物", 1500 }, { "其他", 1500 } };
        private Dictionary<string, int> NewSinglePriceMapping = new Dictionary<string, int>() { { "語文", 300 }, { "生物", 300 } };
        private Dictionary<string, int> OldSinglePriceMapping = new Dictionary<string, int>() { { "語文", 270 }, { "生物", 270 } };
        string yearMonth = (DateTime.Now.Year - 1911).ToString() + DateTime.Now.Month.ToString();

        private readonly IWeekTimeService _weekTimeServiceService;
        private readonly IStudentService _studentServiceService;
        public TuitioneService(little_beanContext littleBeanContext, IWeekTimeService weekTimeService, IStudentService studentServiceService)
        {
            _littleBeanContext = littleBeanContext;
            _weekTimeServiceService = weekTimeService;
            _studentServiceService = studentServiceService;
        }

        public List<TuitionInfo> GetCountStudentInfo(int[] studrntId, string yearMonth)
        {
            List<TuitionInfo> GetCountStudentInfo = new List<TuitionInfo>();
            foreach (var id in studrntId)
            {
                var joinResult = from StudentInfo in _littleBeanContext.StudentInfos
                                 from Course in _littleBeanContext.Courses
                                 from StudentCourse in _littleBeanContext.StudentCourses
                                 from Meal in _littleBeanContext.Meals
                                 join MealWeek in _littleBeanContext.MealWeeks
                                 on Meal.Student equals MealWeek.Student into gj
                                 from MealWeek in gj.DefaultIfEmpty()
                                 join MealDate in _littleBeanContext.MealDates
                                 on Meal.Student equals MealDate.Student into gj2
                                 from MealDate in gj2.DefaultIfEmpty()
                                 join CourseTime in _littleBeanContext.CourseTimes
                                     on Course.Id equals CourseTime.CourseId into gj3
                                 from CourseTime in gj3.DefaultIfEmpty()
                                 where StudentInfo.Id == id && StudentInfo.Id == StudentCourse.Student && Course.Id == StudentCourse.Course || (MealWeek.YearMonth == yearMonth || MealDate.YearMonth == yearMonth)
                                 select new TuitionQueryResult { StudentInfo = StudentInfo, Course = Course, CourseTime = CourseTime, StudentCourse = StudentCourse, Meal = Meal, MealWeek = MealWeek, MealDate = MealDate };

                var result = joinResult.Select(x => new TuitionInfo
                {
                    StudentId = x.StudentInfo.Id,
                    CourseSelect = x.StudentCourse.Course,
                    CourseName = x.Course.Name,
                    CourseTime = x.CourseTime.WeekDay,
                    MealWeek = x.MealWeek.Week,
                    StudentType = Enum.Parse<EnumStudentType>(x.StudentInfo.Type.ToString(), true),
                    PayType = Enum.Parse<EnumPayType>(x.StudentInfo.PayType.ToString(), true),
                    MealPayType = Enum.Parse<EnumMealPayType>(x.Meal.Type.ToString(), true),
                    CourseType = Enum.Parse<EnumCourseType>(x.Course.CourseType.ToString(), true),
                    ClassType = Enum.Parse<EnumClassType>(x.Course.ClassType.ToString(), true),
                    StudentCourseStatus = System.Enum.Parse<EnumStudentCourseStatus>(x.StudentCourse.Status.ToString(), true),
                    //MealCount = x.MealDate.Count
                }).ToList();
                GetCountStudentInfo.AddRange(result);
            }
            return GetCountStudentInfo;
        }

        public List<TuitionInfo> GetStudentInfo(int[] studrntId, string yearMonth)
        {
            List<TuitionInfo> GetStudentInfo = new List<TuitionInfo>();
            foreach (var id in studrntId)
            {
                var joinResult = from StudentInfo in _littleBeanContext.StudentInfos
                                 from Course in _littleBeanContext.Courses
                                 from StudentCourse in _littleBeanContext.StudentCourses
                                 from Meal in _littleBeanContext.Meals
                                 join MealWeek in _littleBeanContext.MealWeeks
                                 on Meal.Student equals MealWeek.Student into gj
                                 from MealWeek in gj.DefaultIfEmpty()
                                 join MealDate in _littleBeanContext.MealDates
                                 on Meal.Student equals MealDate.Student into gj2
                                 from MealDate in gj2.DefaultIfEmpty()
                                 where StudentInfo.Id == id && StudentInfo.Id == StudentCourse.Student && Course.Id == StudentCourse.Course || (MealWeek.YearMonth == yearMonth || MealDate.YearMonth == yearMonth)
                                 select new TuitionQueryResult { StudentInfo = StudentInfo, Course = Course, StudentCourse = StudentCourse, Meal = Meal, MealWeek = MealWeek, MealDate = MealDate };

                var result = joinResult.Select(x => new TuitionInfo
                {
                    StudentId = x.StudentInfo.Id,
                    StudentGrade = x.StudentInfo.Grade,
                    CourseSelect = x.StudentCourse.Course,
                    CourseName = x.Course.Name,
                    MealWeek = x.MealWeek.Week,
                    StudentType = Enum.Parse<EnumStudentType>(x.StudentInfo.Type.ToString(), true),
                    PayType = Enum.Parse<EnumPayType>(x.StudentInfo.PayType.ToString(), true),
                    MealPayType = Enum.Parse<EnumMealPayType>(x.Meal.Type.ToString(), true),
                    CourseType = Enum.Parse<EnumCourseType>(x.Course.CourseType.ToString(), true),
                    ClassType = Enum.Parse<EnumClassType>(x.Course.ClassType.ToString(), true),
                    StudentCourseStatus = System.Enum.Parse<EnumStudentCourseStatus>(x.StudentCourse.Status.ToString(), true),
                    //MealCount = x.MealDate.Count
                }).ToList();
                GetStudentInfo.AddRange(result);
            }
            return GetStudentInfo;
        }

        public List<TuitionInfo> CalculateTuition(int[] studrntId, bool isInterrupt)
        {
            var CalculateTuitionResult = new List<TuitionInfo>();
            var studenCountInfo = GetCountStudentInfo(studrntId, yearMonth);
            var studenInfo = GetStudentInfo(studrntId, yearMonth);
            var currentStudentId = 0;

            var partialAmount = 0;
            var tuition = 0;
            var courseCount = 0;
            var courseSelectCount = 0;
            var meal = 0;
            var mealWeek = "";
            var first = true;
            var weekTime = _weekTimeServiceService.CalculateWeekTime(DateTime.Now.AddMonths(1).AddDays(-DateTime.Now.AddMonths(1).Day));
            var weekTimeInterrupt = _weekTimeServiceService.CalculateWeekTime(DateTime.Now);


            foreach (var item in studenInfo)
            {
                //學費:判斷是不是未滿一個月，是的話採單堂else採月
                //     若單堂則計算該月有幾堂課
                //     判斷新生、舊生；繳費方式、上幾科，是否有預繳、並給予優惠
                if (first == true)
                {
                    currentStudentId = item.StudentId;
                    first = false;
                }

                if (isInterrupt == false && item.PayType != EnumPayType.計次繳)
                {
                    tuition = MajorMonthPriceMapping[item.CourseName];

                    if (currentStudentId != item.StudentId)
                    {
                        switch (courseCount)
                        {
                            case 2:
                                partialAmount -= 300;
                                break;
                            case 3:
                                partialAmount -= 600;
                                break;
                            case 4:
                                if (item.StudentGrade == 7)
                                {
                                    partialAmount -= 1900;
                                }
                                else
                                {
                                    partialAmount -= 1400;
                                }
                                break;
                            default:
                                partialAmount = 8500;
                                break; ;
                        }

                        int[] currentStudentIds = currentStudentId.ToString().Select(s => int.Parse(s.ToString())).ToArray();
                        meal = CalculateMeal(currentStudentIds, isInterrupt);
                        item.Total = partialAmount;
                        CalculateTuitionResult.Add(item);
                        courseSelectCount = 0;
                        partialAmount = 0;
                    }
                    else
                    {
                        courseSelectCount += 1;
                        partialAmount += tuition;
                    }
                }
                else
                {
                    foreach (var item2 in studenCountInfo)
                    {
                        if (isInterrupt == false)
                        {
                            switch (item2.StudentType)
                            {
                                case EnumStudentType.新生:
                                    courseCount = weekTime[Convert.ToInt32(item2.CourseTime)];
                                    tuition += NewSinglePriceMapping[item2.CourseName] * courseCount;
                                    break;
                                case EnumStudentType.舊生:
                                    courseCount = weekTime[Convert.ToInt32(item2.CourseTime)];
                                    tuition += OldSinglePriceMapping[item2.CourseName] * courseCount;
                                    break;
                            }
                        }
                        else
                        {
                            switch (item.StudentType)
                            {
                                case EnumStudentType.新生:
                                    courseCount = weekTimeInterrupt[Convert.ToInt32(item2.CourseTime)];
                                    tuition += NewSinglePriceMapping[item2.CourseName] * courseCount;
                                    break;
                                case EnumStudentType.舊生:
                                    courseCount = weekTimeInterrupt[Convert.ToInt32(item2.CourseTime)];
                                    tuition += OldSinglePriceMapping[item2.CourseName] * courseCount;
                                    break;
                            }
                        }
                    }
                }
            }
            //判斷學生身分，若是低收則總學費打五折

            //教材費:判斷月份是否為2、7月，若是則一科600教材費


            return CalculateTuitionResult;
        }
        public int CalculateMeal(int[] studrntId, bool isInterrupt)
        {
            var calculateMealResult = 0;
            var weekTime = _weekTimeServiceService.CalculateWeekTime(DateTime.Now.AddMonths(1).AddDays(-DateTime.Now.AddMonths(1).Day));
            var weekTimeInterrupt = _weekTimeServiceService.CalculateWeekTime(DateTime.Now);
            var getMealInfo = GetStudentInfo(studrntId, yearMonth);
            //餐費:判斷是包月還是計次
            //     若是包月則計算該月吃幾天
            var week = getMealInfo[0].MealWeek.Split(',');
            var weekTotalCount = 0;
            foreach (var weekCount in week)
            {
                if (isInterrupt == false)
                {
                    weekTotalCount += weekTime[Convert.ToInt32(weekCount)];
                }
                else
                {
                    weekTotalCount += weekTimeInterrupt[Convert.ToInt32(weekCount)];
                }
            }
            calculateMealResult = weekTotalCount * 75;
            /*if (item.MealPayType.ToString() == "月繳")
            {
                var week = item.MealWeek.Split(',');
                var weekTotalCount = 0;
                foreach (var weekCount in week)
                {
                    weekTotalCount += weekTime[Convert.ToInt32(weekCount)];
                }
                meal = weekTotalCount * 75;
            }
            else if (item.MealPayType.ToString() == "計次")
            {
                var mealCount = item.MealCount;
                meal = mealCount * 75;
            }
            else
            {
                meal = 0;
            }*/
            return calculateMealResult;
        }
    }

    internal class TuitionQueryResult
    {
        public StudentInfo StudentInfo { get; set; }
        public Course Course { get; set; }
        public StudentCourse StudentCourse { get; set; }
        public Meal Meal { get; set; }
        public MealWeek MealWeek { get; set; }
        public MealDate MealDate { get; set; }
        public CourseTime CourseTime { get; set; }
    }

}
