﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Interface;
using little_bean_toll.Models;
using Microsoft.EntityFrameworkCore;

namespace little_bean_toll.Service
{
    public class WeekTimeService : IWeekTimeService
    {
        private readonly little_beanContext _littleBeanContext;
        public WeekTimeService(little_beanContext littleBeanContext)
        {
            _littleBeanContext = littleBeanContext;
        }
        #region class weekdays
        public int[] Weekdays(DateTime dtmStart, DateTime dtmEnd)
        {
            int dowStart = (int)dtmStart.DayOfWeek;
            TimeSpan tSpan = dtmEnd - dtmStart;
            int[] WeekCount = { 0, 0, 0, 0, 0, 0, 0 };
            int Week = tSpan.Days / 7;
            int WeekMod = tSpan.Days % 7;

            for (int i = 1; i <= Week; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    WeekCount[j] += 1;
                }
            }
            for (int k = dowStart; k <= dowStart + WeekMod; k++)
            {
                WeekCount[k] += 1;
            }
            return WeekCount;
        }
        #endregion

        public List<int> CalculateWeekTime(DateTime dateTime)
        {
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;

            DateTime firstDayOfThisMonth = new DateTime(year, month, 1);
            DateTime toDayOfThisMonth = new DateTime(year, month, dateTime.Day);
            DateTime lastDayOfThisMonth = new DateTime(year, month, DateTime.DaysInMonth(year, month));
            int[] result = lastDayOfThisMonth.Subtract(dateTime.Date).Days < 7 ? Weekdays(firstDayOfThisMonth, toDayOfThisMonth) : Weekdays(firstDayOfThisMonth, lastDayOfThisMonth);

            return result.ToList();
        }
    }
}
