﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.IO.Image;
using System.IO;
using iText.Kernel.Font;
using iText.Forms;
using iText.Kernel.Utils;

namespace little_bean_toll.Service
{
    public class GeneratePdfDemo
    {
        public static void Print()
        {
            string[] name = {"A", "B", "C", "D", "E"};
            var outputPath = @"D:\temp\";
            PdfDocument pdf = new PdfDocument(new PdfWriter(@"D:\小豆芽繳費單\" + (DateTime.Now.Year - 1911).ToString()+"年" + DateTime.Now.ToString("MM") + "月繳費單.pdf"));
            for (int i = 0; i <= 4; i++)
            {
                var dic = new Dictionary<string, string>();
                dic.Add("Name", name[i]);
                dic.Add("Education", "中");
                dic.Add("Class", "一年");
                dic.Add("Year", "111");
                dic.Add("Month", "4");
                dic.Add("Chinese", "2300");
                dic.Add("Group1", "0");
                dic.Add("Total", "2300");
                var filebyte = PDFUtil.FillForm(@"D:\template.pdf", dic);
                using (FileStream fs = new FileStream(outputPath + "result" + (i + 1).ToString() + ".pdf", FileMode.OpenOrCreate, FileAccess.Write))
                {
                    fs.Write(filebyte, 0, filebyte.Length);
                }

                PdfMerger merger = new PdfMerger(pdf);

                //Add pages from the first document
                PdfDocument sourcePdf = new PdfDocument(new PdfReader(outputPath + "result" + (i + 1).ToString() + ".pdf"));
                merger.Merge(sourcePdf, 1, sourcePdf.GetNumberOfPages());
                sourcePdf.Close();

            }
            pdf.Close();
            DeleteAllPdf(outputPath);
        }
        public static class PDFUtil
        {
            /// <summary>
            /// 使用Dictionary填充PDF表單
            /// </summary>
            /// <param name="templatePath">PDF表單模板地址</param>
            /// <param name="Dic">要填充的數據</param>
            /// <returns>PDF文件字節流</returns>
            public static byte[] FillForm(String templatePath, Dictionary<String, String> Dic)
            {
                var ms = new MemoryStream();
                using (PdfDocument document = new PdfDocument(new PdfReader(templatePath), new PdfWriter(ms)))
                {
                    //使用內置字體
                    PdfFont font = PdfFontFactory.CreateFont("C://Windows/Fonts/kaiu.ttf", iText.IO.Font.PdfEncodings.IDENTITY_H);
                    //使用系統字體
                    PdfFont systemfont = PdfFontFactory.CreateFont("C://Windows/Fonts/arial.ttf", iText.IO.Font.PdfEncodings.IDENTITY_H);
                    //獲取表單
                    var form = PdfAcroForm.GetAcroForm(document, true);
                    //獲取表單域
                    var fields = form.GetFormFields();
                    //遍歷表單域進行填充
                    foreach (var field in fields)
                    {
                        //在Dictionary中查找有無對應域的數據
                        if (Dic.TryGetValue(field.Key, out string value))
                        {
                            //填充表單、設置字體、設置只讀
                            field.Value
                                .SetValue(value)
                                .SetFont(systemfont)
                                .SetFont(font)
                                .SetReadOnly(true);
                        }
                    }
                }
                return ms.ToArray();
            }
        }
        public static void DeleteAllPdf(string Directorypath)
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Directorypath);
            if (di.Exists)
            {
                FileInfo[] ff = di.GetFiles("*.pdf");
                foreach (FileInfo temp in ff)
                {
                    File.Delete(Directorypath + "\\" + temp.Name);
                }
            }
        }
    }
}