﻿using System.Collections.Generic;
using little_bean_toll.Models;
using Microsoft.EntityFrameworkCore;

namespace little_bean_toll.Interface
{
    public interface IStudentService
    {
        List<StudentResponse> CalculateStudentGrade(DbSet<StudentInfo> studentInfos);
        string GetStudentInfo(int Id);
        int GetStudentInfo(string Name);
    }
}