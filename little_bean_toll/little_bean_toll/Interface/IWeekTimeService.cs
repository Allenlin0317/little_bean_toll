﻿using System;
using System.Collections.Generic;
using little_bean_toll.Models;
using Microsoft.EntityFrameworkCore;

namespace little_bean_toll.Interface
{
    public interface IWeekTimeService
    {
        List<int> CalculateWeekTime(DateTime dateTime);
    }
}