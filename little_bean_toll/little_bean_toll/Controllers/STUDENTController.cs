﻿using little_bean_toll.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using little_bean_toll.Interface;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace little_bean_toll.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly little_beanContext _littleBeanContext;
        private readonly IStudentService _studentServiceService;

        public StudentController(little_beanContext littleBeanContext, IStudentService studentServiceService)
        {
            _littleBeanContext = littleBeanContext;
            _studentServiceService = studentServiceService;
        }
 
        // GET: api/<STUDENTController>
        [HttpGet]
        public IEnumerable<StudentResponse> GetAll()
        {
            return _studentServiceService.CalculateStudentGrade(_littleBeanContext.StudentInfos);
        }

        // GET api/<STUDENTController>/5
        [HttpGet]
        public List<StudentResponse> QueryStudentInfos([FromQuery] int grade, string name = "", string classes = "", string school = "", string parentName = "")
        {
            return _studentServiceService.CalculateStudentGrade(_littleBeanContext.StudentInfos).Where(a => (string.IsNullOrEmpty(name) || a.Name.Contains(name)) &&
                                                              (string.IsNullOrEmpty(classes) || a.Class.Contains(classes)) &&
                                                              (string.IsNullOrEmpty(school) || a.School.Contains(school)) &&
                                                              (string.IsNullOrEmpty(parentName) || a.ParentsName.Contains(parentName)) &&
                                                              (grade == 0 || a.Grade==grade)).ToList();
        }

        // POST api/<STUDENTController>
        [HttpPost]
        public ActionResult<StudentInfo> Post([FromBody] StudentInfo value)
        {
            _littleBeanContext.StudentInfos.Add(value);
            _littleBeanContext.SaveChanges();

            return Ok(value.Id);
        }

        // PUT api/<STUDENTController>/5
        [HttpPut("{id}")]
        public ActionResult<StudentInfo> Put(int id, [FromBody] StudentInfo value)
        {
            value.ModifiedOn = DateTime.Now;
            _littleBeanContext.Entry(value).State = EntityState.Modified;

            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (!_littleBeanContext.StudentInfos.Any(e => e.Id == id))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return NoContent();
        }

        // DELETE api/<STUDENTController>/5
        [HttpDelete("{id}")]
        public ActionResult<StudentInfo> Delete(int id)
        {
            var del = _littleBeanContext.StudentInfos.Find(id);
            if (del == null)
            {
                return NotFound("查無該筆資料無法刪除");
            }

            _littleBeanContext.StudentInfos.Remove(del);
            _littleBeanContext.SaveChanges();

            return NoContent();
        }
    }
}
