﻿using little_bean_toll.Dtos;
using little_bean_toll.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Enums;
using little_bean_toll.Interface;
using little_bean_toll.Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace little_bean_toll.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly little_beanContext _littleBeanContext;

        public CourseController(little_beanContext littleBeanContext)
        {
            _littleBeanContext = littleBeanContext;
        }

        // GET: api/<CourseController>
        [HttpGet]
        public List<CourseInfo> GetAll()
        {
            var joinResult = from Course in _littleBeanContext.Courses
                join CourseTime in _littleBeanContext.CourseTimes
                    on Course.Id equals CourseTime.CourseId into gj
                from CourseTime in gj.DefaultIfEmpty()
                join Teacher in _littleBeanContext.Teachers
                    on Course.TeacherId equals Teacher.Id into gj2
                from Teacher in gj2.DefaultIfEmpty()
                select new CourseInfo
                {
                    Id=Course.Id,
                    ClassType = Enum.Parse<EnumClassType>(Course.ClassType.ToString(), true),
                    CourseType = Enum.Parse<EnumCourseType>(Course.CourseType.ToString(), true),
                    CourseName = Course.Name,
                    Grade = Course.Grade,
                    TeacherName = Teacher.Name,
                    TeacherId = (Course.TeacherId == null? 0: (int) Course.TeacherId),
                    CourseTime =
                        new CourseTime()
                        {
                            CourseId = Course.Id,
                            WeekDay = CourseTime.WeekDay,
                            StartTime = CourseTime.StartTime,
                            EndTime = CourseTime.EndTime
                        }
                };

            return joinResult.ToList();
        }

        public List<CourseInfo> GetCourseNoCourseTime()
        {
            var joinResult = from Course in _littleBeanContext.Courses
                             join Teacher in _littleBeanContext.Teachers
                                 on Course.TeacherId equals Teacher.Id into gj2
                             from Teacher in gj2.DefaultIfEmpty()
                             select new CourseInfo
                             {
                                 Id = Course.Id,
                                 ClassType = Enum.Parse<EnumClassType>(Course.ClassType.ToString(), true),
                                 CourseType = Enum.Parse<EnumCourseType>(Course.CourseType.ToString(), true),
                                 CourseName = Course.Name,
                                 Grade = Course.Grade,
                                 TeacherName = Teacher.Name,
                                 TeacherId = Course.TeacherId ?? 0,
                             };

            return joinResult.ToList();
        }

        // GET api/<CourseController>/5
        [HttpGet]
        public List<CourseInfo> QueryCourses([FromQuery] int grade, string courseName = "",
            string teacherName = "", string week = "")
        {
            var joinResult = from Course in _littleBeanContext.Courses
                join CourseTime in _littleBeanContext.CourseTimes
                    on Course.Id equals CourseTime.CourseId into gj
                from CourseTime in gj.DefaultIfEmpty()
                join Teacher in _littleBeanContext.Teachers
                    on Course.TeacherId equals Teacher.Id into gj2
                from Teacher in gj2.DefaultIfEmpty()
                where (string.IsNullOrEmpty(courseName) || Course.Name.Contains(courseName)) &&
                      (string.IsNullOrEmpty(teacherName) || Teacher.Name.Contains(teacherName)) &&
                      (string.IsNullOrEmpty(week) || CourseTime.WeekDay.Contains(week)) &&
                      (grade == 0 || Course.Grade == grade)
                select new CourseInfo
                {
                    ClassType = Enum.Parse<EnumClassType>(Course.ClassType.ToString(), true),
                    CourseType = Enum.Parse<EnumCourseType>(Course.CourseType.ToString(), true),
                    CourseName = Course.Name,
                    Grade = Course.Grade,
                    CourseTime =
                        new CourseTime()
                        {
                            WeekDay = CourseTime.WeekDay,
                            StartTime = CourseTime.StartTime,
                            EndTime = CourseTime.EndTime
                        }
                };

            return joinResult.ToList();
        }

        // POST api/<CourseController>
        [HttpPost]
        public ActionResult<CourseInfo> Post(CourseInfo value)
        {
            var CourseValue = new Course
            {
                Grade = value.Grade,
                Name = value.CourseName,
                CourseType = (int) value.CourseType,
                ClassType = (int) value.ClassType,
                TeacherId = value.TeacherId
            };


            _littleBeanContext.Courses.Add(CourseValue);
            _littleBeanContext.SaveChanges();

            var courseTimes = new List<CourseTime> {value.CourseTime};
            var courseValueId = CourseValue.Id;
            AssignCourseIdToCourseTime(courseTimes, courseValueId);

            _littleBeanContext.CourseTimes.AddRange(courseTimes);
            _littleBeanContext.SaveChanges();


            return Ok(value.Id);
        }

        private static void AssignCourseIdToCourseTime(List<CourseTime> courseTimes, int courseValueId)
        {
            foreach (var courseTime in courseTimes)
            {
                courseTime.CourseId = courseValueId;
            }
        }


        // POST api/<CourseController>
        [HttpPost]
        public ActionResult<CourseInfo> CourseSelection([FromBody] CourseInfo value)
        {
            foreach (var item in value.StudentCourseId)
            {
                var studentCourseValue = new StudentCourse
                {
                    Student = value.StudentId,
                    Course = item,
                    Status = 1
                };
                _littleBeanContext.StudentCourses.Add(studentCourseValue);
            }

            _littleBeanContext.SaveChanges();

            return Ok(value.Id);
        }

        // PUT api/<CourseController>/5
        [HttpPut("{id:int}")]
        public ActionResult<Course> Put(int id, [FromBody] Course value)
        {
            value.ModifiedOn = DateTime.Now;
            value.Id = id;
            _littleBeanContext.Entry(value).State = EntityState.Modified;

            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException ex)
            {                
                if (!_littleBeanContext.Courses.Any(e => e.Id == value.Id))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }

            return NoContent();
        }

        // PUT api/<CourseController>/5
        [HttpPut]
        public ActionResult<CourseInfo> PutCourseSelection([FromBody] CourseInfo value)
        {
            var QueryMeal = (from StudentCourse in _littleBeanContext.StudentCourses
                where StudentCourse.Student.Equals(value.StudentId)
                select new {StudentCourse}).ToList();
            QueryMeal.First().StudentCourse.Status = Convert.ToInt32(value.StudentCourseStatus);
            QueryMeal.First().StudentCourse.ModifiedOn = DateTime.Now;

            _littleBeanContext.Entry(value).State = EntityState.Modified;

            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (!_littleBeanContext.StudentCourses.Any(e => e.Student == value.StudentId))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }

            return NoContent();
        }

        // DELETE api/<CourseController>/5
        [HttpDelete("{id}")]
        public ActionResult<Course> Delete(int id)
        {
            var del = _littleBeanContext.Courses.Find(id);
            if (del == null)
            {
                return NotFound("查無該筆資料無法刪除");
            }

            _littleBeanContext.Courses.Remove(del);
            _littleBeanContext.SaveChanges();

            return NoContent();
        }
    }
}