﻿using little_bean_toll.Dtos;
using little_bean_toll.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Interface;
using little_bean_toll.Service;
using little_bean_toll.Enums;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace little_bean_toll.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TuitionController : ControllerBase
    {
        private readonly little_beanContext _littleBeanContext;
        private readonly IStudentService _studentServiceService;

        public TuitionController(little_beanContext littleBeanContext, IStudentService studentServiceService)
        {
            _littleBeanContext = littleBeanContext;
            _studentServiceService = studentServiceService;
        }
        // GET: api/<MealController>
        [HttpGet]
        public IQueryable<Tuition> GetAll()
        {
            return _littleBeanContext.Tuitions;
        }

        // GET api/<MealController>/5
        [HttpGet]
        public List<TuitionInfo> GetStudentInfo([FromQuery]int[] studrntId, string yearMonth)
        {
            List<TuitionInfo> GetStudentInfo = new List<TuitionInfo>();
            foreach (var id in studrntId)
            {
                var joinResult = from StudentInfo in _littleBeanContext.StudentInfos
                                 from Course in _littleBeanContext.Courses
                                 from StudentCourse in _littleBeanContext.StudentCourses
                                 from Meal in _littleBeanContext.Meals
                                 join MealWeek in _littleBeanContext.MealWeeks
                                 on Meal.Student equals MealWeek.Student into gj
                                 from MealWeek in gj.DefaultIfEmpty()
                                 join MealDate in _littleBeanContext.MealDates
                                 on Meal.Student equals MealDate.Student into gj2
                                 from MealDate in gj2.DefaultIfEmpty()
                                 where StudentInfo.Id == id && StudentInfo.Id == StudentCourse.Student && Course.Id == StudentCourse.Course || (MealWeek.YearMonth == yearMonth || MealDate.YearMonth == yearMonth)
                                 select new TuitionQueryResult { StudentInfo = StudentInfo, Course = Course, StudentCourse = StudentCourse, Meal = Meal, MealWeek = MealWeek, MealDate = MealDate };

                var result = joinResult.Select(x => new TuitionInfo
                {
                    StudentId = x.StudentInfo.Id,
                    CourseSelect = x.StudentCourse.Course,
                    CourseName = x.Course.Name,
                    MealWeek = x.MealWeek.Week,
                    StudentType = Enum.Parse<EnumStudentType>(x.StudentInfo.Type.ToString(), true),
                    PayType = Enum.Parse<EnumPayType>(x.StudentInfo.PayType.ToString(), true),
                    MealPayType = Enum.Parse<EnumMealPayType>(x.Meal.Type.ToString(), true),
                    CourseType = Enum.Parse<EnumCourseType>(x.Course.CourseType.ToString(), true),
                    ClassType = Enum.Parse<EnumClassType>(x.Course.ClassType.ToString(), true),
                    StudentCourseStatus = System.Enum.Parse<EnumStudentCourseStatus>(x.StudentCourse.Status.ToString(), true),
                    //MealCount = x.MealDate.Count
                }).ToList();
                GetStudentInfo.AddRange(result);
            }
            return GetStudentInfo;
        }

        // POST api/<MealController>
        [HttpPost]
        public ActionResult<Meal> Post([FromBody] Meal value, string name, string week)
        {
            int StudentId = _studentServiceService.GetStudentInfo(name);
            MealWeek WeekValue = new MealWeek();

            if (StudentId != 0)
            {
                value.Student = StudentId;
            }
            else
            {
                return NotFound("查無該學生");
            }
            _littleBeanContext.Meals.Add(value);

            if (value.Type == (int)EnumMealPayType.月繳)
            {
                WeekValue.Student = value.Student;
                WeekValue.Week = week;

                _littleBeanContext.MealWeeks.Add(WeekValue);
            }

            _littleBeanContext.SaveChanges();
            return Ok();
        }

        // POST api/<MealController>
        [HttpPost]
        public ActionResult<MealDateInfo> PostMealDate([FromBody] MealDateInfo[] Datevalue)
        {
            for (int i = 0; i <= Datevalue.Length; i++)
            {
                var QueryId = from StudentInfo in _littleBeanContext.StudentInfos
                              where StudentInfo.Name.Contains(Datevalue[i].name)
                              select new { StudentInfo.Id };
                int StudentId = QueryId.FirstOrDefault().Id;

                if (StudentId != 0)
                {
                    Datevalue[i].Student = StudentId;
                }
                else
                {
                    return StatusCode(500, Datevalue[i] + "非餐點計次學生");
                }
                if (_littleBeanContext.MealDates.Any(e => e.Student == Datevalue[i].Student && e.YearMonth == Datevalue[i].YearMonth))
                {
                    return StatusCode(500, Datevalue[i].name + "已有訂餐紀錄");
                }
                Datevalue[i].Date = DateTime.Now.ToString("dd");
                Datevalue[i].YearMonth = (DateTime.Now.Year - 1911).ToString("yyyMM");

                var postValue = new MealDate
                {
                    Student = Datevalue[i].Student,
                    Date = Datevalue[i].Date,
                    YearMonth = Datevalue[i].YearMonth,
                    Count = 1
                };
                _littleBeanContext.MealDates.Add(postValue);
            }
            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return Ok();
        }
        // POST api/<MealController>
        [HttpPost]
        public ActionResult<MealDateInfo> CancelMealDate([FromBody] MealDateInfo[] Datevalue)
        {
            for (int i = 0; i <= Datevalue.Length; i++)
            {
                Datevalue[i].Date = DateTime.Now.ToString("dd");
                Datevalue[i].YearMonth = (DateTime.Now.Year - 1911).ToString("yyyMM");

                var postValue = new MealDate
                {
                    Student = Datevalue[i].Student,
                    Date = Datevalue[i].Date,
                    YearMonth = Datevalue[i].YearMonth,
                    Count = -1
                };
                _littleBeanContext.MealDates.Add(postValue);
            }
            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return Ok();
        }

        // PUT api/<MealController>/5
        [HttpPut]
        public ActionResult<MealDateInfo> Put([FromBody] MealDateInfo value)
        {
            value.ModifiedOn = DateTime.Now;
            _littleBeanContext.Entry(value).State = EntityState.Modified;

            var QueryMeal = (from Meal in _littleBeanContext.Meals
                             where Meal.Student.Equals(value.Student)
                             select new { Meal }).ToList();

            switch (value.Type)
            {
                case (int)EnumMealPayType.月繳 when QueryMeal[0].Meal.Type != value.Type:
                    {
                        MealWeek weekValue = new MealWeek
                        {
                            Student = value.Student,
                            Week = value.Week
                        };
                        QueryMeal.First().Meal.Type = value.Type;

                        _littleBeanContext.MealWeeks.Add(weekValue);
                        break;
                    }
                case (int)EnumMealPayType.計次 when QueryMeal.First().Meal.Type != value.Type:
                    QueryMeal.First().Meal.Type = value.Type;
                    break;
                case (int)EnumMealPayType.月繳 when QueryMeal.First().Meal.Type == value.Type:
                    {
                        var QueryWeek = (from MealWeek in _littleBeanContext.MealWeeks
                                         where MealWeek.Student.Equals(value.Student)
                                         select new { MealWeek }).ToList();

                        QueryWeek.First().MealWeek.Week = value.Week;
                        break;
                    }
            }
            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return NoContent();
        }

        // PUT api/<MealController>/5
        [HttpPut("{id}")]
        public ActionResult<MealWeek> PutMealWeek([FromBody] MealWeek value)
        {
            _littleBeanContext.Entry(value).State = EntityState.Modified;
            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "存取發生錯誤");
            }
            return NoContent();
        }

        // DELETE api/<MealController>/5
        [HttpDelete("{id}")]
        public ActionResult<Meal> Delete(int id)
        {
            var del = _littleBeanContext.Meals.Find(id);
            if (del == null)
            {
                return NotFound("查無該筆資料無法刪除");
            }

            _littleBeanContext.Meals.Remove(del);
            _littleBeanContext.SaveChanges();

            return NoContent();
        }
    }
}
