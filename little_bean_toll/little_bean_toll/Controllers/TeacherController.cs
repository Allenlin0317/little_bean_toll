﻿using little_bean_toll.Dtos;
using little_bean_toll.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Interface;
using little_bean_toll.Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace little_bean_toll.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TeacherController : ControllerBase
    {
        private readonly little_beanContext _littleBeanContext;

        public TeacherController(little_beanContext littleBeanContext)
        {
            _littleBeanContext = littleBeanContext;
        }
        // GET: api/<TeacherController>
        [HttpGet]
        public IQueryable<Teacher> GetAll()
        {
            return _littleBeanContext.Teachers;
        }

        // GET api/<TeacherController>/5
        [HttpGet]
        public List<TeacherInfo> QueryTeachers([FromQuery] string courseName = "", string teacherName = "", string week = "")
        {
            var joinResult = from Teacher in _littleBeanContext.Teachers
                             join Course in _littleBeanContext.Courses
                             on Teacher.Id equals Course.TeacherId into gj
                             from Course in gj.DefaultIfEmpty()
                             join CourseTime in _littleBeanContext.CourseTimes
                             on Course.Id equals CourseTime.CourseId into gj2
                             from CourseTime in gj2.DefaultIfEmpty()
                             where (string.IsNullOrEmpty(courseName) || Course.Name.Contains(courseName)) &&
                             (string.IsNullOrEmpty(teacherName) || Teacher.Name.Contains(teacherName)) &&
                             (string.IsNullOrEmpty(week) || CourseTime.WeekDay.Contains(week))
                             select new TeacherInfo
                             {
                                 TeacherName = Teacher.Name,
                                 TeacherTel =Teacher.Tel,
                                 CourseName = Course.Name,
                                 CourseTime = new List<CourseTime>()
                {
                    new CourseTime()
                    {
                        WeekDay = CourseTime.WeekDay,
                        StartTime = CourseTime.StartTime,
                        EndTime = CourseTime.EndTime
                    }
                }
                             };
            return joinResult.ToList();
        }

        // POST api/<TeacherController>
        [HttpPost]
        public ActionResult<Teacher> Post([FromBody] Teacher value)
        {
            _littleBeanContext.Teachers.Add(value);
            _littleBeanContext.SaveChanges();

            return Ok(value.Id);
        }

        // PUT api/<TeacherController>/5
        [HttpPut("{id}")]
        public ActionResult<Teacher> Put(int id, [FromBody] Teacher value)
        {
            value.ModifiedOn = DateTime.Now;
            value.Id = id;
            _littleBeanContext.Entry(value).State = EntityState.Modified;

            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (!_littleBeanContext.Teachers.Any(e => e.Id == value.Id))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return NoContent();
        }

        // DELETE api/<TeacherController>/5
        [HttpDelete("{id}")]
        public ActionResult<Teacher> Delete(int id)
        {
            var del = _littleBeanContext.Teachers.Find(id);
            if (del == null)
            {
                return NotFound("查無該筆資料無法刪除");
            }

            _littleBeanContext.Teachers.Remove(del);
            _littleBeanContext.SaveChanges();

            return NoContent();
        }
    }
}
