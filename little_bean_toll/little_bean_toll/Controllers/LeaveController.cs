﻿using little_bean_toll.Dtos;
using little_bean_toll.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Interface;
using little_bean_toll.Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace little_bean_toll.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LeaveController : ControllerBase
    {
        private readonly little_beanContext _littleBeanContext;
        private readonly IStudentService _studentServiceService;

        public LeaveController(little_beanContext littleBeanContext, IStudentService studentServiceService)
        {
            _littleBeanContext = littleBeanContext;
            _studentServiceService = studentServiceService;
        }
        // GET: api/<LeaveController>
        [HttpGet]
        public IQueryable<Leave> GetAll()
        {
            return _littleBeanContext.Leaves;
        }

        // GET api/<LeaveController>/5
        [HttpGet]
        public List<Leave> QueryLeaves([FromQuery] string Name = "", string LeaveDate = "")
        {
            int StudentId = _studentServiceService.GetStudentInfo(Name);

            return _littleBeanContext.Leaves.Where(a => (string.IsNullOrEmpty(Name) || (a.Student == StudentId)) &&
                            (string.IsNullOrEmpty(LeaveDate) || a.LeaveDate.Contains(LeaveDate.Replace("/", "")))).ToList();
        }

        // POST api/<LeaveController>
        [HttpPost]
        public ActionResult<Leave> Post([FromBody] Leave value, string name)
        {
            int StudentId = _studentServiceService.GetStudentInfo(name);

            if (StudentId != 0)
            {
                value.Student = StudentId;
            }
            else
            {
                return NotFound("查無該學生"); ;
            }

            value.LeaveDate = value.LeaveDate.Replace("/", "");
            value.YearMonth = value.LeaveDate.Substring(0, 4) + value.LeaveDate.Substring(3, 2);

            if (_littleBeanContext.Leaves.Any(e => e.LeaveCourse == value.LeaveCourse && e.LeaveDate == value.LeaveDate))
            {
                return StatusCode(500, "已有此筆請假紀錄");
            }
            _littleBeanContext.Leaves.Add(value);
            _littleBeanContext.SaveChanges();

            return Ok();
        }

        // PUT api/<LeaveController>/5
        [HttpPut("{id}")]
        public ActionResult<Leave> Put(int id, [FromBody] Leave value)
        {
            value.ModifiedOn = DateTime.Now;
            value.Id = id;
            _littleBeanContext.Entry(value).State = EntityState.Modified;

            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (!_littleBeanContext.Leaves.Any(e => e.Id == value.Id))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return NoContent();
        }

        // DELETE api/<LeaveController>/5
        [HttpDelete("{id}")]
        public ActionResult<Leave> Delete(int id)
        {
            var del = _littleBeanContext.Leaves.Find(id);
            if (del == null)
            {
                return NotFound("查無該筆資料無法刪除");
            }

            _littleBeanContext.Leaves.Remove(del);
            _littleBeanContext.SaveChanges();

            return NoContent();
        }
    }
}
