﻿using little_bean_toll.Dtos;
using little_bean_toll.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Enums;
using little_bean_toll.Interface;
using little_bean_toll.Service;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace little_bean_toll.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StudentCourseController : ControllerBase
    {
        private readonly little_beanContext _littleBeanContext;

        public StudentCourseController(little_beanContext littleBeanContext)
        {
            _littleBeanContext = littleBeanContext;
        }

        // GET api/<StudentCourseController>/5
        [HttpGet]
        public List<CourseInfo> QueryStudentCourses([FromQuery] int studentId)
        {
            var joinResult1 = from StudentCourse in _littleBeanContext.StudentCourses
                join CourseTime in _littleBeanContext.CourseTimes
                    on StudentCourse.Course equals CourseTime.CourseId into gj
                from CourseTime in gj.DefaultIfEmpty()
                join Course in _littleBeanContext.Courses
                    on StudentCourse.Course equals Course.Id
                where (StudentCourse.Student == studentId)
                select new CourseInfo
                {
                    ClassType = Enum.Parse<EnumClassType>(Course.ClassType.ToString(), true),
                    CourseType = Enum.Parse<EnumCourseType>(Course.CourseType.ToString(), true),
                    StudentCourseStatus = Enum.Parse<EnumStudentCourseStatus>(StudentCourse.Status.ToString(), true).ToString(),
                    CourseName = Course.Name,
                    Grade = Course.Grade,
                    StatusReason = StudentCourse.StatusReason,
                    CourseTime =
                        new CourseTime()
                        {
                            WeekDay = CourseTime.WeekDay,
                            StartTime = CourseTime.StartTime,
                            EndTime = CourseTime.EndTime
                        }
                };
            var result = joinResult1.ToList();
            return joinResult1.ToList();
        }

        // POST api/<StudentCourseController>
        [HttpPost]
        public ActionResult<CourseInfo> Post([FromBody] CourseInfo value)
        {
            foreach (var item in value.StudentCourseId)
            {
                var StudentCourseValue = new StudentCourse
                {
                    Student = value.StudentId,
                    Course = item,
                    Status = (int)EnumStudentCourseStatus.上課
                };
                _littleBeanContext.StudentCourses.Add(StudentCourseValue);
            }

            _littleBeanContext.SaveChanges();
            return Ok(value.Id);
        }

        // PUT api/<StudentCourseController>/5
        [HttpPut]
        public ActionResult Put([FromBody] StudentCourse value)
        {
            var update = _littleBeanContext.StudentCourses.First(x => x.Student == value.Student && x.Course == value.Course);
            update.Status = value.Status;
            update.StatusReason = value.StatusReason;
            update.ModifiedOn = DateTime.Now;

            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (!_littleBeanContext.StudentCourses.Any(e => e.Student == value.Student || e.Course == value.Course))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }

            return NoContent();
        }

        // DELETE api/<StudentCourseController>/5
        [HttpDelete]
        public ActionResult Delete(StudentCourse value)
        {
            var del = _littleBeanContext.StudentCourses.First(x => x.Student == value.Student && x.Course == value.Course);
            if (del == null)
            {
                return NotFound("查無該筆資料無法刪除");
            }

            _littleBeanContext.StudentCourses.Remove(del);
            _littleBeanContext.SaveChanges();

            return NoContent();
        }
    }
}