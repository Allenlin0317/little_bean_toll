﻿using little_bean_toll.Dtos;
using little_bean_toll.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using little_bean_toll.Interface;
using little_bean_toll.Service;
using little_bean_toll.Enums;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace little_bean_toll.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MealController : ControllerBase
    {
        private readonly little_beanContext _littleBeanContext;
        private readonly IStudentService _studentServiceService;

        public MealController(little_beanContext littleBeanContext, IStudentService studentServiceService)
        {
            _littleBeanContext = littleBeanContext;
            _studentServiceService = studentServiceService;
        }
        // GET: api/<MealController>
        [HttpGet]
        public IQueryable<Meal> GetAll()
        {
            return _littleBeanContext.Meals;
        }

        // GET api/<MealController>/5
        [HttpGet]
        public List<MealInfo> QueryMeals([FromQuery] MealInfo value)
        {
            switch (value.Type)
            {
                case EnumMealPayType.不訂餐:
                    {
                        var joinResult = from Meal in _littleBeanContext.Meals
                                         where (value.Student == 0 || Meal.Student == value.Student)
                                         select new MealInfo { Student = Meal.Student, Type = Enum.Parse<EnumMealPayType>(Meal.Type.ToString()) };
                        return joinResult.ToList();
                    }
                case EnumMealPayType.月繳:
                    {
                        var joinResult = from Meal in _littleBeanContext.Meals
                                         join MealWeek in _littleBeanContext.MealWeeks
                                             on Meal.Student equals MealWeek.Student
                                         where (value.Student == 0 || Meal.Student == value.Student) &&
                                               (string.IsNullOrEmpty(value.YearMonth) || MealWeek.YearMonth.Contains(value.YearMonth))
                                         select new MealInfo { Student = Meal.Student, Week = MealWeek.Week, Type = Enum.Parse<EnumMealPayType>(Meal.Type.ToString()), YearMonth = MealWeek.YearMonth };
                        return joinResult.ToList();
                    }
                case EnumMealPayType.計次:
                    {
                        var joinResult = from Meal in _littleBeanContext.Meals
                                         join MealDate in _littleBeanContext.MealDates
                                             on Meal.Student equals MealDate.Student
                                         where (value.Student == 0 || Meal.Student == value.Student) &&
                                               (MealDate.YearMonth.Contains(value.YearMonth))
                                         select new MealInfo { Student = Meal.Student, Date = MealDate.Date, Type = Enum.Parse<EnumMealPayType>(Meal.Type.ToString()), YearMonth = MealDate.YearMonth };
                        return joinResult.ToList();
                    }
                default:
                    {
                        var joinResult = from Meal in _littleBeanContext.Meals
                                         where (value.Student == 0 || Meal.Student == value.Student)
                                         select new MealInfo { Student = Meal.Student, Type = Enum.Parse<EnumMealPayType>(Meal.Type.ToString()) };
                        return joinResult.ToList();
                    }
            }
        }

        // POST api/<MealController>
        [HttpPost]
        public ActionResult<MealInfo> Post([FromBody] MealInfo value)
        {
            var queryStudent = _littleBeanContext.StudentInfos.FirstOrDefault(x => x.Id == value.Student);
            if (queryStudent == null)
            {
                return NotFound("查無該學生");
            }
            var queryMealStudent = _littleBeanContext.Meals.FirstOrDefault(x => x.Student == value.Student);
            if (queryMealStudent != null)
            {
                return NotFound("已有該學生訂餐資料");
            }
            var meals = new Meal
            {
                Student = value.Student,
                Type = (int)Enum.Parse<EnumMealPayType>(value.Type.ToString(), true)
            };
            _littleBeanContext.Meals.Add(meals);

            if (value.Type == EnumMealPayType.月繳)
            {
                var weeks = "";
                for (var i = 1; i < value.Week.Length; i++)
                {
                    weeks += value.Week.Substring(i - 1, 1) + ",";
                }
                value.Week = weeks + value.Week.Substring(value.Week.Length - 1, 1);
                var mealWeek = new MealWeek
                {
                    Student = value.Student,
                    Week = value.Week,
                    YearMonth = (DateTime.Now.Year - 1911).ToString() + DateTime.Now.ToString("MM")
                };
                _littleBeanContext.MealWeeks.Add(mealWeek);
            }

            _littleBeanContext.SaveChanges();
            return Ok();
        }

        // POST api/<MealController>
        [HttpPost]
        public ActionResult<MealDateInfo> PostMealDate([FromBody] MealDateInfo[] Datevalue)
        {
            for (int i = 0; i <= Datevalue.Length; i++)
            {
                var QueryId = from StudentInfo in _littleBeanContext.StudentInfos
                              where StudentInfo.Name.Contains(Datevalue[i].name)
                              select new { StudentInfo.Id };
                int StudentId = QueryId.FirstOrDefault().Id;

                if (StudentId != 0)
                {
                    Datevalue[i].Student = StudentId;
                }
                else
                {
                    return StatusCode(500, Datevalue[i] + "非餐點計次學生");
                }
                if (_littleBeanContext.MealDates.Any(e => e.Student == Datevalue[i].Student && e.YearMonth == Datevalue[i].YearMonth))
                {
                    return StatusCode(500, Datevalue[i].name + "已有訂餐紀錄");
                }
                Datevalue[i].Date = DateTime.Now.ToString("dd");
                Datevalue[i].YearMonth = (DateTime.Now.Year - 1911).ToString("yyyMM");

                var postValue = new MealDate
                {
                    Student = Datevalue[i].Student,
                    Date = Datevalue[i].Date,
                    YearMonth = Datevalue[i].YearMonth,
                    Count = 1
                };
                _littleBeanContext.MealDates.Add(postValue);
            }
            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return Ok();
        }
        // POST api/<MealController>
        [HttpPost]
        public ActionResult<MealDateInfo> CancelMealDate([FromBody] MealDateInfo[] Datevalue)
        {
            for (int i = 0; i <= Datevalue.Length; i++)
            {
                Datevalue[i].Date = DateTime.Now.ToString("dd");
                Datevalue[i].YearMonth = (DateTime.Now.Year - 1911).ToString("yyyMM");

                var postValue = new MealDate
                {
                    Student = Datevalue[i].Student,
                    Date = Datevalue[i].Date,
                    YearMonth = Datevalue[i].YearMonth,
                    Count = -1
                };
                _littleBeanContext.MealDates.Add(postValue);
            }
            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return Ok();
        }

        // PUT api/<MealController>/5
        [HttpPut("{Id:int}")]
        public ActionResult<Meal> Put([FromBody] MealInfo value)
        {
            var update = _littleBeanContext.Meals.First(x => x.Student == value.Student);
            update.ModifiedOn = DateTime.Now;
            update.Type = (int)value.Type;

            if (value.Type == EnumMealPayType.月繳 && update.Type != (int)value.Type)
            {
                var weeks = "";
                for (var i = 1; i < value.Week.Length; i++)
                {
                    weeks += value.Week.Substring(i - 1, 1) + ",";
                }

                value.Week = weeks + value.Week.Substring(value.Week.Length - 1, 1);
                var weekValue = new MealWeek
                {
                    Student = value.Student,
                    Week = value.Week,
                    YearMonth = (DateTime.Now.Year - 1911).ToString() + DateTime.Now.ToString("MM")
                };

                _littleBeanContext.MealWeeks.Add(weekValue);
            }
            else if (value.Type == EnumMealPayType.月繳 && update.Type == (int)value.Type)
            {
                var weeks = "";
                for (var i = 1; i < value.Week.Length; i++)
                {
                    weeks += value.Week.Substring(i - 1, 1) + ",";
                }

                value.Week = weeks + value.Week.Substring(value.Week.Length - 1, 1);

                var updateMealWeek = _littleBeanContext.MealWeeks.First(x => x.Student == value.Student);
                updateMealWeek.Week = value.Week;
            }

            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                {
                    return StatusCode(500, "存取發生錯誤");
                }
            }
            return NoContent();
        }

        // PUT api/<MealController>/5
        [HttpPut("{id}")]
        public ActionResult<MealWeek> PutMealWeek([FromBody] MealWeek value)
        {
            _littleBeanContext.Entry(value).State = EntityState.Modified;
            try
            {
                _littleBeanContext.SaveChanges();
            }
            catch (DbUpdateException)
            {
                return StatusCode(500, "存取發生錯誤");
            }
            return NoContent();
        }

        // DELETE api/<MealController>/5
        [HttpDelete("{id}")]
        public ActionResult<Meal> Delete(int id)
        {
            var del = _littleBeanContext.Meals.Find(id);
            if (del == null)
            {
                return NotFound("查無該筆資料無法刪除");
            }

            _littleBeanContext.Meals.Remove(del);
            _littleBeanContext.SaveChanges();

            return NoContent();
        }
    }
}
