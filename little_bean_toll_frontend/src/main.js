import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import installElementPlus from './plugins/element'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEdit, faAlignLeft } from '@fortawesome/free-solid-svg-icons'


const app = createApp(App)
library.add(faEdit, faAlignLeft)
app.component('font-awesome-icon', FontAwesomeIcon)
installElementPlus(app)
app.use(store).use(router).mount('#app')