import { createStore } from 'vuex'

export default createStore({
  state: {
    navBarStatus: false
  },
  mutations: {
    SET_NavBar_Status (state, status) {
      state.navBarStatus = status;
    }
  },
  actions: {
  },
  modules: {
  }
})
