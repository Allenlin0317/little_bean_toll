import ElementPlus from 'element-plus'
import { Expand } from '@element-plus/icons'
import '../element-variables.scss'
import locale from 'element-plus/lib/locale/lang/zh-tw'

export default (app) => {
  app.use(ElementPlus, { locale })
  app.component('expand',Expand)
}
