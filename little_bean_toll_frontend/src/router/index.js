import { createRouter, createWebHistory } from 'vue-router'
import Login from '../components/Login.vue'
import StudentMgmt from '../components/StudentMgmt'
import StudentQuery from '../components/StudentQuery'
import CourseQuery from '../components/CourseQuery'
import CourseMgmt from '../components/CourseMgmt'
import TeacherQuery from '../components/TeacherQuery'
import TeacherMgmt from '../components/TeacherMgmt'
import About from '../views/About'
import Nav from '../components/NavBar.vue'

const routes = [
  {
    path: '/',
    name: 'default',
    components: { default: StudentMgmt, nav: Nav }
  },
  {
    path: '/Login',
    name: 'Login',
    components: {
      default: Login,
    }
  },
  {
    path: '/StudentMgmt',
    name: 'StudentMgmt',
    components: {
      default: StudentMgmt,
      nav: Nav
    }
  },
  {
    path: '/StudentQuery',
    name: 'StudentQuery',
    components: {
      default: StudentQuery,
      nav: Nav
    }
  },
  {
    path: '/CourseMgmt',
    name: 'CourseMgmt',
    components: {
      default: CourseMgmt,
      nav: Nav
    }
  },
  {
    path: '/CourseQuery',
    name: 'CourseQuery',
    components: {
      default: CourseQuery,
      nav: Nav
    }
  },
  {
    path: '/TeacherMgmt',
    name: 'TeacherMgmt',
    components: {
      default: TeacherMgmt,
      nav: Nav
    }
  },
  {
    path: '/TeacherQuery',
    name: 'TeacherQuery',
    components: {
      default: TeacherQuery,
      nav: Nav
    }
  },
  {
    path: '/3',
    name: '3',
    components: {
      default: About,
      nav: Nav
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
